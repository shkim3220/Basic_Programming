#include <stdio.h>

int main(void)
{
  int x;
  int y;
  int sum;
  
  printf("Input x : ");
  scanf("%d",&x);
  printf("Input y : ");
  scanf("%d",&y);
  
  sum = x + y;
  
  printf("Sum of x and y : %d\n",sum);

  return 0;
}