#include <stdio.h>

int main(void)
{
  char name[20];
  
  printf("What is your name ? ");
  scanf("%s",name);
  
  printf("Hello, %s! Nice to meet you!\n",name);

  return 0;
}